df <- read.csv("plot.dat", sep = ";", header=T, comment.char = "")
df$timestamp <- strptime(df$timestamp, "%Y-%m-%d %H:%M:%S")

type <- c("X.user", "X.nice", "X.system", "X.iowait")
label <- c("%user", "%nice", "%system", "%iowait")
color <- rainbow(4)
pch <- seq.int(1, 4)

bitmap("sar-R.png", type="png16m", units="px", width=800, height=500,
       res=150, taa=4, gaa=4)

for (i in 3:1) {
    df[[type[i]]] <- df[[type[i]]] + df[[type[i+1]]]
}

par(las=2)
plot(df$timestamp,
    df$X.user,
    type = "b",
    col = color[1],
    ylim = c(0, max(df$X.user, na.rm = T)),
    main="Processor Utilization",
    xlab="Time",
    ylab="% Utilization")

for (i in 2:4) {
    points(df$timestamp, df[[type[i]]], type = "b", pch = pch[i],
           col=color[i])
}

legend('topright', label, pch=pch, col=color)
grid(col="gray")
dev.off()
