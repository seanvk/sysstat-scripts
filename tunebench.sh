#!/bin/sh

#
# A script to toggle power management between high performance and very low power usage.
#
#

##
## POWER SAVE OFF
##

ac_power()
{
  ##
  ## DISK and FILESYSTEMS
  ##

  rm -rf /var/tmp/powr_save_on
  touch /var/tmp/powr_save_off

  # Remount ext3/4 filesystems so the journal commit only happens every 60
  # seconds.  By default this is 5 but, I prefer to reduce the disk
  # activity a bit.
  mount -o remount,commit=60,atime /

  # Set the SATA to max performance
  for i in 0 1 2 3 4 5 ; do
      echo 'max_performance'>/sys/class/scsi_host/host${i}/link_power_management_policy
  done

  ##
  ## DEVICES
  ##

  # Full power for generic devices
  for i in `find /sys/devices/*/power/control` ; do
      echo 'on' > ${i}
  done

  # Full power for PCI devices
  for i in `find /sys/bus/pci/devices/*/power/control` ; do
      echo 'on' > ${i}
  done

  # Full power for USB devices
  for i in `find /sys/bus/usb/devices/*/power/control` ; do
      echo 'on' > ${i}
  done

  # Full power for i2c devices
  for i in `find /sys/bus/i2c/devices/*/device/power/control` ; do
      echo 'on' > ${i}
  done
  
  # Disable autosuspend power-management for USB devices
  for i in `find /sys/bus/usb/devices/*/power/autosuspend` ; do
      echo '0' > ${i}
  done


  ##
  ## CPU AND MEMORY
  ##

  # Disable ASLR
  sudo sysctl -w kernel.randomize_va_space=0

  # Drop caches
  echo 3 > /proc/sys/vm/drop_caches

  # Set kernel dirty page value back to default
  echo '10' > /proc/sys/vm/dirty_ratio
  echo '5' > /proc/sys/vm/dirty_background_ratio

  # Only wakeup every 60 seconds to see if we need to write dirty pages
  # By default this is every 5 seconds but, I prefer 60 to reduce disk
  # activity.
  echo '6000' > /proc/sys/vm/dirty_writeback_centisecs
  #echo 15 > /proc/sys/vm/dirty_writeback_centisecs

  # Make sure ondemand governor is set
  /sbin/modprobe cpufreq_ondemand > /dev/null 2>&1
  # Use the "performance" CPU scaling
  for i in /sys/devices/system/cpu/cpu?/cpufreq/scaling_governor ; do
      echo 'performance' > ${i}
  done


  # Disable the NMI watchdog
  echo '0' > /proc/sys/kernel/watchdog


  ##
  ## Wired
  ##
  #ethtool -s enp0s25 wol d;

  # Bump networking connection tracking size
  echo 512000 | sudo tee /proc/sys/net/netfilter/nf_conntrack_max

  # Give the kernel the ability to reuse TCP ports which may be in a
  # TIME_WAIT state before kicking off the test
  echo 1 | sudo tee /proc/sys/net/ipv4/tcp_tw_reuse

  # JEMALLOC cache purge decay rate
  export MALLOC_CONF=muzz_decay_ms:-1,dirty_decay_ms:-1

  # HW CRC, likely duplicated in your build script
  export COMPILER_FLAGS="-march=armv8-a+crc --sysroot=/"
}

##
## POWER SAVE ON
##

save_power()
{
  ##
  ## DISK and FILESYSTEMS
  ##

  rm -rf /var/tmp/powr_save_off
  touch /var/tmp/powr_save_on
  
  # Change ext3/ext4 filesystem settings to reduce disk activity.
  # noatime => disable updates to a file's access time when the file is read.
  # commit=600 => Change the commit times to 10 minutes.
  mount -o remount,noatime,commit=600 /

  # Reduce power for SATA link power management
  for i in 0 1 2 3 4 5 ; do
      echo 'min_power'>/sys/class/scsi_host/host${i}/link_power_management_policy
  done

  ##
  ## DEVICES
  ##

  # Runtime power-management for generic devices
  for i in `find /sys/devices/*/power/control` ; do
      echo 'auto' > ${i}
  done

  # Runtime power-management for PCI devices
  for i in `find /sys/bus/pci/devices/*/power/control` ; do
      echo 'auto' > ${i}
  done

  # Runtime power-management for USB devices
  for i in `find /sys/bus/usb/devices/*/power/control` ; do
      echo 'auto' > ${i}
  done

  # Runtime autosuspend power-management for USB devices
  for i in `find /sys/bus/usb/devices/*/power/autosuspend` ; do
      echo '2' > ${i}
  done

  ##
  ## CPU AND MEMORY
  ##

  # Disable the NMI watchdog
  echo '0' > /proc/sys/kernel/watchdog

  # Use the "ondemand" CPU governor
  /sbin/modprobe cpufreq_ondemand > /dev/null 2>&1
  # Use the "powersave" CPU scaling
  for i in /sys/devices/system/cpu/cpu?/cpufreq/scaling_governor ; do
      echo 'powersave' > ${i}
  done

  # Reduce disk activity by waiting up to 10 minutes before doing writes
  echo '60' > /proc/sys/vm/dirty_ratio
  echo '40' > /proc/sys/vm/dirty_background_ratio
  #sleep 30 && echo 1500 > /proc/sys/vm/dirty_writeback_centisecs
  echo '1500' > /proc/sys/vm/dirty_writeback_centisecs

  ##
  ## SOUND AND VIDEO
  ##

  # Set sound card power savings
  echo 'Y' > /sys/module/snd_hda_intel/parameters/power_save_controller
  echo '1' > /sys/module/snd_hda_intel/parameters/power_save

  # Remove the webcam driver
  #modprobe -r uvcvideo

  ##
  ## Wireless
  ##
  #hciconfig hci0 down #; rmmod hci_usb
  #iwconfig wlan0 power on

  ##
  ## Wired
  ##
  #ethtool -s enp0s25 wol d;
}

##
## APPLY SETTINGS
##

case "$1" in
  false) ac_power ;;
  true) save_power ;;
esac

exit 0
